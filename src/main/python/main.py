import subprocess


def main():
    shared_output_dest = "/Users/serhiiholovko/Documents/dsv-leader-election/src/main/resources/shared_output.txt";

    topologies = open('/Users/serhiiholovko/Documents/dsv-leader-election/src/main/resources/topologies.txt', 'r')
    lines = topologies.readlines()

    original_command = "/Users/serhiiholovko/Documents/jdk-11.0.2.jdk/Contents/Home/bin/java --illegal-access=deny -Dfile.encoding=UTF-8 -classpath /Users/serhiiholovko/Documents/dsv-leader-election/target/classes:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-netty/1.16.1/grpc-netty-1.16.1.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-core/1.16.1/grpc-core-1.16.1.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-context/1.16.1/grpc-context-1.16.1.jar:/Users/serhiiholovko/.m2/repository/com/google/code/gson/gson/2.7/gson-2.7.jar:/Users/serhiiholovko/.m2/repository/com/google/errorprone/error_prone_annotations/2.2.0/error_prone_annotations-2.2.0.jar:/Users/serhiiholovko/.m2/repository/com/google/code/findbugs/jsr305/3.0.2/jsr305-3.0.2.jar:/Users/serhiiholovko/.m2/repository/org/codehaus/mojo/animal-sniffer-annotations/1.17/animal-sniffer-annotations-1.17.jar:/Users/serhiiholovko/.m2/repository/io/opencensus/opencensus-api/0.12.3/opencensus-api-0.12.3.jar:/Users/serhiiholovko/.m2/repository/io/opencensus/opencensus-contrib-grpc-metrics/0.12.3/opencensus-contrib-grpc-metrics-0.12.3.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec-http2/4.1.30.Final/netty-codec-http2-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec-http/4.1.30.Final/netty-codec-http-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec/4.1.30.Final/netty-codec-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-handler/4.1.30.Final/netty-handler-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-buffer/4.1.30.Final/netty-buffer-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-common/4.1.30.Final/netty-common-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-handler-proxy/4.1.30.Final/netty-handler-proxy-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-transport/4.1.30.Final/netty-transport-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-resolver/4.1.30.Final/netty-resolver-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec-socks/4.1.30.Final/netty-codec-socks-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-protobuf/1.16.1/grpc-protobuf-1.16.1.jar:/Users/serhiiholovko/.m2/repository/com/google/protobuf/protobuf-java/3.5.1/protobuf-java-3.5.1.jar:/Users/serhiiholovko/.m2/repository/com/google/guava/guava/26.0-android/guava-26.0-android.jar:/Users/serhiiholovko/.m2/repository/org/checkerframework/checker-compat-qual/2.5.2/checker-compat-qual-2.5.2.jar:/Users/serhiiholovko/.m2/repository/com/google/j2objc/j2objc-annotations/1.1/j2objc-annotations-1.1.jar:/Users/serhiiholovko/.m2/repository/com/google/api/grpc/proto-google-common-protos/1.0.0/proto-google-common-protos-1.0.0.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-protobuf-lite/1.16.1/grpc-protobuf-lite-1.16.1.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-stub/1.16.1/grpc-stub-1.16.1.jar:/Users/serhiiholovko/.m2/repository/jakarta/annotation/jakarta.annotation-api/1.3.5/jakarta.annotation-api-1.3.5.jar NodeMain"
    original_command_array = original_command.split()
    processes = []
    f = open("measuring.txt", "w")
    f.write("Nodes amount -> messages count\n")

    for l in range(100):
        line = lines[l]
        nodes = line.strip('\n').split(',')

        for i in range(len(nodes) - 1):
            # Start a java application for every node
            bashCmd = []
            bashCmd.extend(original_command_array)
            bashCmd.extend([nodes[i], nodes[i + 1]])
            process = subprocess.Popen(bashCmd)
            processes.append(process)

        bashCmd = []
        bashCmd.extend(original_command_array)
        bashCmd.extend([nodes[-1], nodes[0]])
        process = subprocess.Popen(bashCmd)
        processes.append(process)

        output = [p.wait() for p in processes]

        num_lines = sum(1 for line in open(shared_output_dest)) - 1
        f.write(str(len(nodes)) + " -> " + str(num_lines) + "\n")

    f.close()


if __name__ == "__main__":
    main()
