#!/bin/bash

cd /Users/serhiiholovko/Documents/dsv-leader-election/target

start_node="/Users/serhiiholovko/Documents/jdk-11.0.2.jdk/Contents/Home/bin/java --illegal-access=deny -Dfile.encoding=UTF-8 -classpath /Users/serhiiholovko/Documents/dsv-leader-election/target/classes:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-netty/1.16.1/grpc-netty-1.16.1.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-core/1.16.1/grpc-core-1.16.1.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-context/1.16.1/grpc-context-1.16.1.jar:/Users/serhiiholovko/.m2/repository/com/google/code/gson/gson/2.7/gson-2.7.jar:/Users/serhiiholovko/.m2/repository/com/google/errorprone/error_prone_annotations/2.2.0/error_prone_annotations-2.2.0.jar:/Users/serhiiholovko/.m2/repository/com/google/code/findbugs/jsr305/3.0.2/jsr305-3.0.2.jar:/Users/serhiiholovko/.m2/repository/org/codehaus/mojo/animal-sniffer-annotations/1.17/animal-sniffer-annotations-1.17.jar:/Users/serhiiholovko/.m2/repository/io/opencensus/opencensus-api/0.12.3/opencensus-api-0.12.3.jar:/Users/serhiiholovko/.m2/repository/io/opencensus/opencensus-contrib-grpc-metrics/0.12.3/opencensus-contrib-grpc-metrics-0.12.3.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec-http2/4.1.30.Final/netty-codec-http2-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec-http/4.1.30.Final/netty-codec-http-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec/4.1.30.Final/netty-codec-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-handler/4.1.30.Final/netty-handler-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-buffer/4.1.30.Final/netty-buffer-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-common/4.1.30.Final/netty-common-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-handler-proxy/4.1.30.Final/netty-handler-proxy-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-transport/4.1.30.Final/netty-transport-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-resolver/4.1.30.Final/netty-resolver-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/netty/netty-codec-socks/4.1.30.Final/netty-codec-socks-4.1.30.Final.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-protobuf/1.16.1/grpc-protobuf-1.16.1.jar:/Users/serhiiholovko/.m2/repository/com/google/protobuf/protobuf-java/3.5.1/protobuf-java-3.5.1.jar:/Users/serhiiholovko/.m2/repository/com/google/guava/guava/26.0-android/guava-26.0-android.jar:/Users/serhiiholovko/.m2/repository/org/checkerframework/checker-compat-qual/2.5.2/checker-compat-qual-2.5.2.jar:/Users/serhiiholovko/.m2/repository/com/google/j2objc/j2objc-annotations/1.1/j2objc-annotations-1.1.jar:/Users/serhiiholovko/.m2/repository/com/google/api/grpc/proto-google-common-protos/1.0.0/proto-google-common-protos-1.0.0.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-protobuf-lite/1.16.1/grpc-protobuf-lite-1.16.1.jar:/Users/serhiiholovko/.m2/repository/io/grpc/grpc-stub/1.16.1/grpc-stub-1.16.1.jar:/Users/serhiiholovko/.m2/repository/jakarta/annotation/jakarta.annotation-api/1.3.5/jakarta.annotation-api-1.3.5.jar NodeMain"

$start_node 6 7 &
$start_node 7 5 &
$start_node 5 14 &
$start_node 14 8 &
$start_node 8 12 &
$start_node 12 9 &
$start_node 9 4 &
$start_node 4 10 &
$start_node 10 2 &
$start_node 2 13 &
$start_node 13 3 &
$start_node 3 15 &
$start_node 15 0 &
$start_node 0 11 &
$start_node 11 1 &
$start_node 1 6

