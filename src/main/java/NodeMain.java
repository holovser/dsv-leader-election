import cvut.fit.node.ConcurrencyControl;
import cvut.fit.node.Configuration;
import cvut.fit.node.clients.GrpcClient;
import cvut.fit.node.services.ElectionServiceImpl;
import cvut.fit.node.services.TerminationServiceImpl;
import cvut.fit.node.writer.FileAppender;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class NodeMain {
    public static void main(String[] args) throws IOException, InterruptedException {

        FileAppender fileAppender = new FileAppender(); fileAppender.cleanFileContent();

        int nodeId = Integer.parseInt(args[0]);
        int neighbourId = Integer.parseInt(args[1]);
        int neighbourPort = Configuration.basePort + neighbourId;

        System.out.println("Node ID: " + nodeId);

        Configuration.personalPort = Configuration.basePort + nodeId;
        Configuration.personalNodeId = nodeId;
        Configuration.minId = Configuration.personalNodeId;
        Configuration.neighbourPort = neighbourPort;
        Configuration.neighbourId = neighbourId;

        GrpcClient client = new GrpcClient();

        Thread initialElectionThread = new Thread(() -> {
            try {
                client.sendElectionMessage(Configuration.personalNodeId, Configuration.personalNodeId);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        initialElectionThread.start();
        ConcurrencyControl.activeThread = initialElectionThread;

        Server electionServer = ServerBuilder
                .forPort(Configuration.personalPort)
                .addService(new ElectionServiceImpl())
                .addService(new TerminationServiceImpl())
                .build();

        electionServer.start();
        electionServer.awaitTermination();
    }
}