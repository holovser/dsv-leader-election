package cvut.fit.node.services;

import cvut.fit.grpc.TerminationAcknowledge;
import cvut.fit.grpc.TerminationMessage;
import cvut.fit.grpc.TerminationServiceGrpc;
import cvut.fit.node.ConcurrencyControl;
import cvut.fit.node.Configuration;
import cvut.fit.node.clients.GrpcClient;
import cvut.fit.node.writer.FileAppender;
import io.grpc.stub.StreamObserver;

public class TerminationServiceImpl extends TerminationServiceGrpc.TerminationServiceImplBase {

    private final GrpcClient grpcClient = new GrpcClient();
    private final FileAppender fileAppender = new FileAppender();

    @Override
    public void terminate(TerminationMessage request, StreamObserver<TerminationAcknowledge> responseObserver) {

        System.out.println("Node " + Configuration.personalNodeId + " received termination");

        TerminationAcknowledge response = TerminationAcknowledge.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

        int proxyId = request.getProxyId();

        fileAppender.appendToSharedFile(Configuration.personalNodeId, proxyId, Configuration.neighbourId);

        System.out.println("Sending termination message from " + Configuration.personalNodeId + " to " + Configuration.neighbourPort);
        try {
            grpcClient.sendTerminationMessage(Configuration.personalNodeId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConcurrencyControl.activeThread.interrupt();
            System.out.println("Node " + Configuration.personalNodeId + " acquired minimum " + Configuration.minId );
            System.exit(0);
        }
    }
}
