package cvut.fit.node.services;

import cvut.fit.grpc.ElectionMessage;
import cvut.fit.grpc.ElectionMessageAcknowledge;
import cvut.fit.grpc.ElectionServiceGrpc;
import cvut.fit.node.ConcurrencyControl;
import cvut.fit.node.Configuration;
import cvut.fit.node.clients.GrpcClient;
import cvut.fit.node.writer.FileAppender;
import io.grpc.stub.StreamObserver;

public class ElectionServiceImpl extends ElectionServiceGrpc.ElectionServiceImplBase {

    private final GrpcClient grpcClient = new GrpcClient();
    private final FileAppender fileAppender = new FileAppender();

    @Override
    public void election(ElectionMessage request, StreamObserver<ElectionMessageAcknowledge> responseObserver) {

        int receivedId = request.getSenderId();
        int proxyId = request.getProxyId();

        System.out.println("Node " + Configuration.personalNodeId + " received election message from node " + request.getSenderId());
        ElectionMessageAcknowledge response = ElectionMessageAcknowledge.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

        if (receivedId < Configuration.minId) {
            Configuration.minId = request.getSenderId();
            try {
                System.out.println("Node " + Configuration.personalNodeId + " ;" + " Received smaller id: " + receivedId );

                fileAppender.appendToSharedFile(Configuration.personalNodeId, proxyId, Configuration.neighbourId);
                grpcClient.sendElectionMessage(receivedId, Configuration.personalNodeId);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if (receivedId == Configuration.personalNodeId) {
            fileAppender.appendToSharedFile(Configuration.personalNodeId, proxyId, Configuration.personalNodeId);

            // Terminate all nodes
            System.out.println("Node " + Configuration.personalNodeId + " ;" + " Received own id -> terminating others and myself");

            grpcClient.sendTerminationMessage(Configuration.personalNodeId);
            System.out.println("Node " + Configuration.personalNodeId + " acquired minimum " + Configuration.minId );
            ConcurrencyControl.activeThread.interrupt();
            System.exit(0);
        }
    }
}
