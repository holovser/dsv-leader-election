package cvut.fit.node;

public class Configuration {
    public static int basePort = 8600;
    public static int neighbourPort;
    public static int neighbourId;
    public static int personalPort;
    public static int personalNodeId;
    public static int minId;
    public static String outputFileDest = "/Users/serhiiholovko/Documents/dsv-leader-election/src/main/resources/shared_output.txt";
}
