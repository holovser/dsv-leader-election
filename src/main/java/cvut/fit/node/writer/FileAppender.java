package cvut.fit.node.writer;

import cvut.fit.node.Configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

public class FileAppender {

    public void appendToSharedFile(int nodeId, int senderId, int receiverId) {
        LocalDateTime now = LocalDateTime.now();
        String content = "<" +
                nodeId + ", " +
                now.getHour() + ":" + now.getMinute() + ":" + now.getSecond() + ", " +
                senderId + ", " +
                receiverId +
                ">\n";
        try {
            FileOutputStream fos =
                    new FileOutputStream(Configuration.outputFileDest, true);
            fos.write(content.getBytes(StandardCharsets.UTF_8));
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cleanFileContent() {
        File file = new File(Configuration.outputFileDest);
        file.delete();
    }
}
