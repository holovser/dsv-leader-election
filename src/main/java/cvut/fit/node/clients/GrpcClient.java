package cvut.fit.node.clients;

import cvut.fit.grpc.*;
import cvut.fit.node.ConcurrencyControl;
import cvut.fit.node.Configuration;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.concurrent.TimeUnit;

public class GrpcClient {

    public void sendElectionMessage(int electionMessage, int proxyId) throws InterruptedException {
        System.out.println("Trying to send an election message " + electionMessage + " to port " + Configuration.neighbourPort);
        ConcurrencyControl.electionSendingInProcess = true;
        try {
            ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", Configuration.neighbourPort)
                    .usePlaintext()
                    .build();

            ElectionServiceGrpc.ElectionServiceBlockingStub stub
                    = ElectionServiceGrpc.newBlockingStub(channel);

            ElectionMessageAcknowledge electionMessageAcknowledge = stub.election(ElectionMessage.newBuilder()
                    .setSenderId(electionMessage)
                            .setProxyId(proxyId)
                    .build());

            channel.shutdown();
            System.out.println("Sending election message to " + Configuration.neighbourPort + " was successful");
            ConcurrencyControl.electionSendingInProcess = false;
        } catch (StatusRuntimeException e ) {
            System.out.println(e.getMessage());

            if ( Configuration.minId >= Configuration.personalNodeId ) {
                // If sending message was not successful and better id was not received yet
                // -> sleep for 2 seconds and try again.
                // Mainly made to cope with the situation when neighbour nodes are not started
                TimeUnit.SECONDS.sleep(2);
                sendElectionMessage(electionMessage, proxyId);
            } else {
                ConcurrencyControl.electionSendingInProcess = false;
            }
        }
    }

    public void sendTerminationMessage(int proxyId) {
        while (ConcurrencyControl.electionSendingInProcess) {
            System.out.println("Node " + Configuration.personalNodeId + " waiting for ending election sending");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", Configuration.neighbourPort)
                    .usePlaintext()
                    .build();

            TerminationServiceGrpc.TerminationServiceBlockingStub stub
                    = TerminationServiceGrpc.newBlockingStub(channel);

            TerminationAcknowledge terminationAcknowledge =
                    stub.terminate(TerminationMessage.newBuilder().setProxyId(proxyId)
                    .build());

            channel.shutdown();
        } catch (StatusRuntimeException e ) {}
    }



}
